<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informacao extends Model
{
  protected $fillable =  ['carteira','byte','agencia','posto','conta','descricaoDemonstrativo','instrucoes','aceite','especieDoc'];
  protected $guarded = ['id', 'created_at', 'update_at'];
  protected $table = 'informacoes';
}