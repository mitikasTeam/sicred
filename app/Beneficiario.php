<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiario extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome','endereco','cep','uf','cidade','documento'
    ];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'beneficiarios';
}
