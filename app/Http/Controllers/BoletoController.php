<?php

namespace App\Http\Controllers;

use App\Boleto;
use App\Pagador;
use App\Beneficiario;
use App\Informacao;
use Eduardokum\LaravelBoleto\Pessoa;
use Eduardokum\LaravelBoleto\Boleto\Banco\Sicredi;
use Eduardokum\LaravelBoleto\Boleto\Render\Pdf;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;



class BoletoController extends Controller
{
	/**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	 public function index()
    {
    	
    	return view('boletoform');
    }
   	/*
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {	
        return Validator::make($data, [
            'nome' => 'required|string|max:255|min:3',
            'documento' => 'required|string|min:11',
            'endereco' => 'required|string|min:6',            
            'cep' => 'required|integer',          
            'cidade' => 'required|string|min:6',      
            'uf' => 'required|string|min:2',      
            'valor' => 'required|integer',    
            'data' => 'required||after:today',
        ]);
    }

	
    public function create(Request $request)
    {	
    	$v =  $this->validator($request->all());
	    if ($v->fails())
	    {
	        return redirect()->back()->withInput()->withErrors($v->errors());
	    }else{

		    $pagador = Pagador::where('documento', $request['documento'])->first();

		    if (! ($pagador === null))
		    {
		    	$pagador->update([
		    		'nome' =>$request['nome'],
		            'documento' =>$request['documento'],
		            'endereco' =>$request['endereco'],            
		            'cep' =>$request['cep'],          
		            'cidade' =>$request['cidade'],      
		            'uf' =>$request['uf'],
		        ]);
		    }
		    else
		    {
			    $pagador = Pagador::create([
		    		'nome' =>$request['nome'],
		            'documento' =>$request['documento'],
		            'endereco' =>$request['endereco'],            
		            'cep' =>$request['cep'],          
		            'cidade' =>$request['cidade'],      
		            'uf' =>$request['uf'],
		        ]);
			}

	    	$boleto = New Boleto();
	    	$beneficiario = Beneficiario::where('id',1)->firstOrFail();
	    	$informacao = Informacao::where('id',1)->firstOrFail();
	    	$boleto->beneficiario = $beneficiario; 
	    	$boleto->pagador = $pagador; 
	    	$boleto->informacao = $informacao;
	    	$boleto->dataVencimento = $request['data'];
	    	$boleto->valor = $request['valor'];
	    	$boleto->juros = 0;
	    	$boleto->multa = 0;
	    	$boleto->numeroDocumento = random_int(100, 92299);
	    	$boleto->numero = 1;

	    	$boleto->save(); 

	    	return $this->print($boleto->id);	
		    }
    	
       
    	
    }


    public function print($id)
    {		$boleto = Boleto::where('id',$id)->firstOrFail();

		        $beneficiario = new Pessoa(
		    [
		        'nome'      => $boleto->beneficiario_nome,
		        'endereco'  => $boleto->beneficiario_endereco,
		        'cep'       => $boleto->beneficiario_cep,
		        'uf'        => $boleto->beneficiario_uf,
		        'cidade'    => $boleto->beneficiario_cidade,
		        'documento' => $boleto->beneficiario_documento,
		    ]
		);

		$pagador = new Pessoa(
		    [
		        'nome'      => $boleto->pagador_nome,
		        'endereco'  => $boleto->pagador_endereco,
		        'cep'       => $boleto->pagador_cep,
		        'uf'        => $boleto->pagador_uf,
		        'cidade'    => $boleto->pagador_cidade,
		        'documento' => $boleto->pagador_documento,
		    ]
		);

		$boleto_sicredi = new Sicredi(
		    [
		        'logo'                   => realpath(__DIR__ .'/../../vendor/Eduardokum/LaravelBoleto/logos/748.png'), 
		        'dataVencimento'         => new \Carbon\Carbon($boleto->dataVencimento),
		        'valor'                  => $boleto->valor,
		        'multa'                  => ($boleto->multa > 0) ? $boleto->multa : false,
		        'juros'                  => ($boleto->juros > 0) ? $boleto->juros : false,
		        'numero'                 => $boleto->numero,
		        'numeroDocumento'        => $boleto->id,
		        'pagador'                => $pagador,
		        'beneficiario'           => $beneficiario,
		        'carteira'               => $boleto->carteira,
		        'byte'                   => $boleto->byte,
		        'agencia'                => $boleto->agencia,
		        'posto'                  => $boleto->posto,
		        'conta'                  => $boleto->conta,
		        'descricaoDemonstrativo' => explode('|',$boleto->descricaoDemonstrativo),
		        'instrucoes'             => explode('|',$boleto->instrucoes),
		        'aceite'                 => $boleto->aceite,
		        'especieDoc'             => $boleto->especieDoc,
		    ]
		);
		$pdf = new Pdf();
		$pdf->addBoleto($boleto_sicredi);
		$pdf->showPrint();
		return $pdf->gerarBoleto($pdf::OUTPUT_STANDARD, $save_path = null);
    }
    
}
