<?php

namespace App\Http\Controllers;

use App\Beneficiario;
use Illuminate\Http\Request;

class BeneficiarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $beneficiario = Beneficiario::first();
        return view('beneficiario.index',['beneficiario' => $beneficiario]);
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Beneficiario  $beneficiario
     * @return \Illuminate\Http\Response
     */
    public function edit(Beneficiario $beneficiario)
    {
        $beneficiario = Beneficiario::findOrFail($id);
        return view('beneficiario.edit',compact('beneficiario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Beneficiario  $beneficiario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Beneficiario $beneficiario)
    {
        $beneficiario = Beneficiario::findOrFail($id);
        $beneficiario->nome        = $request->nome;
        $beneficiario->endereco = $request->endereco;
        $beneficiario->cep    = $request->cep;
        $beneficiario->uf       = $request->uf;
        $beneficiario->findOrFail       = $request->findOrFail;
        $beneficiario->documento       = $request->documento;
        $beneficiario->save();
        return redirect()->route('beneficiario.index')->with('message', 'Beneficiario atualizado com sucesso!');
    }
    }

  
}
