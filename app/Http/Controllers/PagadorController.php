<?php

namespace App\Http\Controllers;

use App\Pagador;
use Illuminate\Http\Request;

class PagadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagador = Pagador::orderBy('created_at', 'desc')->paginate(10);
        return view('pagador.index',['pagador' => $pagador]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pagador  $pagador
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pagador $pagador)
    {
        // $pagador = Product::findOrFail($id);
        $pagador->delete();
        return redirect()->route('pagador.index')->with('alert-success','Doador deletado!');
    }
    
}
