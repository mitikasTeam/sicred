<?php

namespace App;
use Pagador;
use Beneficiario;
use Informacao;
use Illuminate\Database\Eloquent\Model;

class Boleto extends Model
{
    protected $fillable = ['pagador_nome','pagador_endereco','pagador_cep','pagador_uf','pagador_cidade','pagador_documento','beneficiario_nome','beneficiario_endereco','beneficiario_cep','beneficiario_uf','beneficiario_cidade','beneficiario_documento','dataVencimento','valor','multa','juros','numero','numeroDocumento','carteira','byte','agencia','posto','conta','descricaoDemonstrativo','instrucoes','aceite','especieDoc'];
    protected $guarded = ['id', 'created_at', 'update_at','id_beneficiario', 'id_pagador'];
    protected $table = 'boletos';


	public function setPagadorAttribute($pagador) {

	    $this->attributes['id_pagador'] = $pagador->id; 
	    $this->attributes['pagador_nome'] = $pagador->nome; 
	    $this->attributes['pagador_endereco'] = $pagador->endereco; 
	    $this->attributes['pagador_cep'] = $pagador->cep; 
	    $this->attributes['pagador_uf'] = $pagador->uf; 
	    $this->attributes['pagador_cidade'] = $pagador->cidade; 
	    $this->attributes['pagador_documento'] = $pagador->documento; 
	}

	public function setBeneficiarioAttribute($beneficiario) {
		
	    $this->attributes['id_beneficiario'] = $beneficiario->id; 
	    $this->attributes['beneficiario_nome'] = $beneficiario->nome; 
	    $this->attributes['beneficiario_endereco'] = $beneficiario->endereco; 
	    $this->attributes['beneficiario_cep'] = $beneficiario->cep; 
	    $this->attributes['beneficiario_uf'] = $beneficiario->uf; 
	    $this->attributes['beneficiario_cidade'] = $beneficiario->cidade; 
	    $this->attributes['beneficiario_documento'] = $beneficiario->documento; 
	    
	}

	public function setInformacaoAttribute($informacao){

		$this->attributes['carteira'] = $informacao->carteira; 
	    $this->attributes['byte'] = $informacao->byte; 
	    $this->attributes['agencia'] = $informacao->agencia; 
	    $this->attributes['posto'] = $informacao->posto; 
	    $this->attributes['conta'] = $informacao->conta; 
	    $this->attributes['descricaoDemonstrativo'] = $informacao->descricaoDemonstrativo; 
	    $this->attributes['instrucoes'] = $informacao->instrucoes; 
	    $this->attributes['aceite'] = $informacao->aceite; 
	    $this->attributes['especieDoc'] = $informacao->especieDoc; 
	}
}
