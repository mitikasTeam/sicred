<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagador extends Model
{
    protected $fillable = ['nome','endereco','cep','uf','cidade','documento'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'pagadores';
}
