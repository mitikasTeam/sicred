<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/boleto', 'BoletoController@index')->name('boleto');
Route::get('/boleto/print/{id}',  'BoletoController@print')->name('print_boleto');
Route::post('/gerarboleto', 'BoletoController@create')->name('gerarboleto');

