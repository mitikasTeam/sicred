<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoletosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pagador_nome');
            $table->string('pagador_endereco');
            $table->string('pagador_cep');
            $table->string('pagador_uf');
            $table->string('pagador_cidade');
            $table->string('pagador_documento');
            $table->string('beneficiario_nome');
            $table->string('beneficiario_endereco');
            $table->string('beneficiario_cep');
            $table->string('beneficiario_uf');
            $table->string('beneficiario_cidade');
            $table->string('beneficiario_documento');
            $table->datetime('dataVencimento');
            $table->float('valor');
            $table->float('multa');
            $table->float('juros');
            $table->integer('numero');
            $table->integer('numeroDocumento')->unique();
            $table->string('carteira');
            $table->string('byte');
            $table->string('agencia');
            $table->string('posto');
            $table->string('conta');
            $table->string('descricaoDemonstrativo');
            $table->string('instrucoes');
            $table->string('aceite');
            $table->string('especieDoc');
            $table->integer('id_beneficiario')->unsigned();
            $table->foreign('id_beneficiario')->references('id')->on('beneficiarios');
            $table->integer('id_pagador')->unsigned();
            $table->foreign('id_pagador')->references('id')->on('pagadores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletos');
    }
}
